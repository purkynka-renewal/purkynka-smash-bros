const CopyWebpackPlugin = require('copy-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const WorkboxPlugin = require('workbox-webpack-plugin');
const webpack = require('webpack');
const CustomPrerenderPlugin = require('./custom-prerender-plugin');
const CustomWebmanifestPlugin = require('./custom-webmanifest-plugin');
const CONFIG = require("./config.json");

module.exports = {
  context: __dirname+"/src/",
  mode: CONFIG.debug ? "development" : "production",
  entry: "./js/index.js",
  module: {
    rules: [
      {
        test: /\.s?(a|c)ss$/,
        use: [
          MiniCssExtractPlugin.loader,
          'cache-loader',
          {loader: 'css-loader', options: {url: false}},
          'sass-loader',
        ],
      },
      {
        test: /(img|fonts)\/.+\.(eot|woff|woff2|ttf|otf|svg|png|jpg|gif)$/,
        use: 'url-loader',
      },
      {
        test: /\.ya?ml$/,
        type: 'json',
        use: 'yaml-loader'
      },
      {
        test: /\.jsx$/,
        exclude: /node_modules/,
        use: [
          "thread-loader",
          "cache-loader",
          {
            loader: "babel-loader",
            options: {
              presets: ["@babel/preset-env", "@babel/preset-react"],
              plugins: [
                ["@babel/plugin-transform-react-jsx", { pragma: "h" }]
              ],
            }
          }
        ]
      },
      {
        include: __dirname+"/excercises",
        use: __dirname+'/custom-excercise-loader.js',
      },
    ],
  },
  output: {
    path: __dirname+"/public/",
    publicPath: this.path,
    filename: "boundle.min.js",
    // chunkFilename: 'js/[id].js',
  },
  resolve: {
    symlinks: false,
    alias: {
      "react": 'preact/compat',
      "react-dom": 'preact/compat',
    },
  },
  stats: {
    warnings: false
  },
  cache: {
    type: "filesystem",
    buildDependencies: {
      config: [__filename]
    }
  },
  plugins: [
    new webpack.ProvidePlugin({
      h: ["preact", "h"],
    }),
    new CleanWebpackPlugin(),
    new MiniCssExtractPlugin({filename: "boundle.min.css"}),
    // Copy SemanticUI assets to static folder
    new CopyWebpackPlugin({patterns: [
      {
        from: '../node_modules/fomantic-ui-sass/src/themes/default/assets/images/',
        to: 'img/'
      },
      {
        from: 'img/',
        to: 'img/'
      },
      {
        from: '../node_modules/@fortawesome/fontawesome-free/webfonts/',
        to: 'fonts/'
      },
      {
        from: 'fonts/',
        to: 'fonts/'
      },
      // {
      //   from: 'favicon.ico',
      //   to: './'
      // },
    ]}),
    new HtmlWebpackPlugin({
      title: CONFIG.sitename,
      scriptLoading: "defer",
      meta: {
        "og:title": CONFIG.sitename,
        "og:description": CONFIG.tagline,
        "og:type": "website",
        "og:determiner": "the",
        // "og:image": "https://zhincore.eu/img/zhincore.png",
        // "og:url": "https://zhincore.eu/",
      },
    }),
    new WorkboxPlugin.GenerateSW({
      cacheId: "purkynkasmashbros",
      cleanupOutdatedCaches: true,
      clientsClaim: true,
      skipWaiting: true,
      navigationPreload: true,
      runtimeCaching: [{
        handler: "StaleWhileRevalidate",
        urlPattern: /.*/,
      }],
      exclude: [
        /fonts\/fa\-.+\.(?!woff2).+$/,
        /img\/.*icon\.x.+\..+$/,/*/ */
      ],
      modifyURLPrefix: { auto: "/" },
     }),
    new CustomPrerenderPlugin("index.html"),
    new CustomWebmanifestPlugin({
      src: "manifest.webmanifest",
      manifest: {
        name: CONFIG.sitename,
        short_name: CONFIG.sitename,
        description: CONFIG.tagline,
      }
    }),
  ],
  optimization: {
    minimize: true,
    minimizer: [
      new TerserPlugin({
        terserOptions: {
          output: {
            comments: false,
          },
        },
        extractComments: false,
      }),
    ],
  },
  experiments: {
    topLevelAwait: true,
  },
};
