const { writeFileSync } = require("fs");
const { JSDOM } = require("jsdom");

module.exports = class PreloadPlugin {
  constructor(entry) {
    this.entry = entry;
  }

  apply(compiler) {
    compiler.hooks.afterEmit.tapPromise('PreloadPlugin', compilation =>
      new Promise((resolve)=>{
        const entryPath = compilation.outputOptions.path+this.entry;
        const jsdom = JSDOM.fromFile(entryPath, {
          runScripts: "dangerously",
          resources: "usable",
        });

        jsdom.then((dom)=>{
          dom.window.prerendered = () => {
            writeFileSync(entryPath, dom.serialize());
            resolve();
          };
        });
      })
    );
  }
}
