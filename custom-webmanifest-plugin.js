const {readFileSync, writeFileSync} = require("fs");
const HtmlWebpackPlugin = require("html-webpack-plugin");

module.exports = class WebmanifestPlugin {
  constructor(options={}) {
    this.options = Object.assign({
      src: null,
      dst: null,
      minify: true,
    }, options);
    this.options.manifest = Object.assign({
      name: "Webpack Application",
      short_name: "Webpack App",
      description: "A Webpack Application",
      lang: "en-UK",
    }, options.manifest || {});

    if (!this.options.dst) this.options.dst = this.options.src;
  }

  apply(compiler) {
    compiler.hooks.compilation.tap('WebmanifestPlugin', (compilation) => {
      HtmlWebpackPlugin.getHooks(compilation).alterAssetTagGroups.tapAsync(
        'WebmanifestPlugin', (data, cb) => {
          const srcPath = compilation.options.context+this.options.src;
          const dstPath = compilation.outputOptions.path+this.options.dst;
          const src = JSON.parse(readFileSync(srcPath).toString());
          Object.assign(src, this.options.manifest);
          writeFileSync(dstPath, JSON.stringify(
            src,
            null,
            this.options.minify ? 0 : 2
          ));

          data.headTags.push(HtmlWebpackPlugin.createHtmlTagObject(
            "link", {rel: "manifest", href: "/"+this.options.dst}
          ));

          cb(null, data);
        }
      )
    });
  }
}
