import { Component } from 'preact';

export default class Screen extends Component{
  constructor(props) {
    super(props);
    this.ref = null;
    this.state = {
      shouldRender: !!props.active,
      active: !!props.active,
    };
  }

  onTransitionEnd(event) {
    if (!this.ref || event.currentTarget != this.ref) return;
    this.setState({ shouldRender: false });
    this.ref.removeEventListener("transitionend", this.onTransitionEnd);
  }

  componentDidUpdate(prevProps) {
    const onTransitionEnd = this.onTransitionEnd.bind(this);

    if (prevProps.active && !this.props.active) {
      this.setState({ active: false });
      if(!this.ref) return this.setState({ shouldRender: false });

      setTimeout(()=>
        this.ref.addEventListener("transitionend", onTransitionEnd),
      10);
    }
    else if (!prevProps.active && this.props.active) {
      this.setState({ shouldRender: true });
      setTimeout(()=>this.setState({ active: true }), 10); // delay for transition
      if(this.ref) {
        this.ref.removeEventListener("transitionend", onTransitionEnd);
      }
    }
  }

  render(props, state) {
    if (!state.shouldRender) return;

    let classes = "screen "+(props.class || "");
    if (state.active) classes += " active";

    return (
      <div class={ classes } ref={v=>(this.ref=v)}>{ props.children }</div>
    );
  }
}
