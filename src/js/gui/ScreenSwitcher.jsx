import { cloneElement, toChildArray } from 'preact';
import Screen from './Screen.jsx';

function ScreenSwitcher({ children, currentScreen }) {
  const screens = [];

  for (const item of toChildArray(children)) {
    if (item.type != Screen || !item.props.name) continue;
    screens.push(cloneElement(item, {
      active: item.props.name == currentScreen,
    }));
  }

  return screens;
}

export { ScreenSwitcher, Screen, ScreenSwitcher as default };
