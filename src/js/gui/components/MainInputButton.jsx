import { Fragment, Component } from 'react';
import { Button, Input } from 'semantic-ui-react';

export default class MainInputButton extends Component {
  constructor(props) {
    super(props);
    this.state = { value: "" };
  }

  onChange(_, {value}) {
    this.setState({ value });
  }

  onSubmit() {
    if (!this.state.value) return;
    (this.props.onClick || (()=>{}))(this.state.value);
  }

  render(props, {value}) {
    props.class = "spaced attachedBottom "+(props.class || "");

    return (
      <Fragment>
        <Input class="attachedTop text-center" placeholder={ props.placeholder }
            fluid onChange={ (...a) => this.onChange(...a) } size="huge"
            name="value" input={ value } />
        <Button fluid inverted size="massive" { ...props }
            onClick={ () => this.onSubmit() }>
          { props.children }
        </Button>
      </Fragment>
    );
  }
}
