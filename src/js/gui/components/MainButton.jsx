import { Button } from 'semantic-ui-react';

export default function MainButton(props) {
  props.class = "spaced "+(props.class || "");
  return (
    <Button fluid inverted size="massive" { ...props }>
      { props.children }
    </Button>
  );
}
