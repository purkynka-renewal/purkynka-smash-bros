import { Component, Fragment } from 'preact';
import { Button, Grid, Image, Header, Segment, Table,
    Progress } from 'semantic-ui-react';
import { __ } from '/lang';

const sharedObj = require('/js/SharedObject');

const UNHOVER_TIMEOUT = 1000;

export default class CharacterSelection extends Component {
  constructor(props) {
    super(props);
    if (!this.props.onSelect) this.props.onSelect = ()=>{};
    this.state = {
      selected: null,
      hovered: null,
    };
    this.unHoverTimeout = null;
  }

  onHover(charName){
    if (this.unHoverTimeout) clearTimeout(this.unHoverTimeout);
    this.unHoverTimeout = setTimeout(
      () => this.setState({ hovered: charName }),
      UNHOVER_TIMEOUT * !charName
    );
  }

  onSelect(charName, event) {
    const selected = this.state.selected == charName ? null : charName;
    this.setState({ selected });
    this.props.onSelect(selected);
    event.currentTarget.blur(); // Fix semantic colors
  }

  render(_, {hovered, selected}) {
    const { gameData } = sharedObj, { characters } = gameData;
    const showChar = hovered ? characters[hovered] :
        (selected ? characters[selected] : null);

    return (
      <Grid divided inverted style={{ height: "100%" }}>
        <Grid.Column width={12}><Grid columns={8} stackable>
          { Object.values(characters).map(({parameters, models}) =>
            <Grid.Column>
              <Button inverted toggle style={{ height: "100%" }}
                  active={ parameters.name == selected }
                  onClick={ (ev)=>this.onSelect(parameters.name, ev) }
                  onMouseEnter={ ()=>this.onHover(parameters.name) }
                  onMouseLeave={ ()=>this.onHover(null) }>
                <Image src={ models.default.src } size="small" />
              </Button>
            </Grid.Column>
          ) }
        </Grid></Grid.Column>

        <Grid.Column width={4}>
          <Header>
            { showChar ? showChar.name : __("choose-char") }
            <Header.Subheader>{ showChar ?
              __("made-by")+" "+showChar.author :
              __("hover-char-to-view")
            }</Header.Subheader>
          </Header>
          { showChar ? <Fragment>
            <p>{ showChar.description[__.getLanguage()] ||
              showChar.description[__.DEFAULT_LANG] }</p>

            <Header as="h4" dividing inverted>{ __("abilities") }</Header>

            <Header as="h4" dividing inverted>{ __("stats") }</Header>
            <Table compact="very" basic="very" collapsing
                style={{ lineHeight: .75, margin: "0 0 0 16px" }}>
              { Object.entries(showChar.stats).map(([name, value]) =>
                <Table.Row>
                  <Table.HeaderCell>
                    { __(name) }
                  </Table.HeaderCell>
                  <Table.Cell colspan={ 1 + isNaN(value) }>
                    { value }
                  </Table.Cell>
                  <Table.Cell style={{ minWidth: "128px" }}>
                    { !isNaN(value) ?
                      <Progress size="tiny" color="teal" class="nomargin"
                      value={ value } total={ gameData.maxStats[name] }
                      autoSuccess={0} />
                    : ""}
                  </Table.Cell>
                </Table.Row>
              ) }
            </Table>
          </Fragment> : "" }
        </Grid.Column>
      </Grid>
    );
  }
}
