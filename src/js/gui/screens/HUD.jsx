import { Fragment } from 'preact';
import { Button } from 'semantic-ui-react';
import { __ } from '/lang';

export default function HUD({ onButton }) {
  return (<Fragment>
    <Button size="large" inverted icon labelPosition="left"
        onClick={ ()=>onButton("exit2menu") }
        style={{ position: "absolute", bottom: "8px", left: "8px" }}>
      <i class="fas fa-door-open icon"></i>
      { __("exit-to-menu") }
    </Button>
  </Fragment>);
}
