import { Container } from 'semantic-ui-react';

export default function _MainScreen(props){
  props.class = "mainscreen "+(props.class || "");

  return (
    <Container {...props}>{ props.children }</Container>
  );
}
