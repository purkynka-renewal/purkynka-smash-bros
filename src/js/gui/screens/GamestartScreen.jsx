import { Component, Fragment } from 'preact';
import { Container, Header, Button, Segment, Label } from 'semantic-ui-react';
import MainScreen from './_MainScreen.jsx';
import CharacterSelection from '../components/CharacterSelection.jsx';
import { __ } from '/lang';

const sharedObj = require('/js/SharedObject');

export default class GamestartScreen extends Component{
  constructor(props) {
    super(props);
    this.state = {
      selectedChar: null,
      gameid: sharedObj.gameid,
      players: {},
      playerLimit: [0, 0],
      ready: false,
    };
    this.selectedChar = null;
    this.handlers = {};
  }

  componentWillMount() {
    this.handlers.onGameinfoUpdate = this.onGameinfoUpdate.bind(this);

    sharedObj.on("gameinfo_update", this.handlers.onGameinfoUpdate);
  }

  componentWillUnmount() {
    sharedObj.off("gameinfo_update", this.handlers.onGameinfoUpdate);
  }

  onGameinfoUpdate(data) {
    const state = { players: data.players };
    if ("playerLimit" in data) state.playerLimit = data.playerLimit;
    this.setState(state);
  }

  onCharSelect(selectedChar) {
    this.setState({ selectedChar });
  }

  onReady(state=true) {
    this.setState({ ready: state });
    sharedObj.trigger("selectedChar", state ? this.state.selectedChar : null);
  }

  render({ onButton }, state) {
    const players = Object.values(state.players);
    const playersUnready = players.filter(p => !p.selectedChar);
    const needPlayers = state.playerLimit[0] - players.length;
    const playerLimitFullfilled = needPlayers <= 0;
    const playersReady = playersUnready.length == 0;
    const playerCountReady = playerLimitFullfilled && playersReady;

    return (
      <MainScreen fluid>
        <Container>
          <Header as="h1" floated="left">{ __("choose-fighter") }</Header>
          <div style={{ float:"right", display:"flex", alignItems:"center" }}>
            { __("players-ready") }:
            <Label color={ playerCountReady ? "green":"red" }>
              { (players.length - playersUnready.length)+
                " / "+
                Object.keys(state.players).length+
                " / "+
                state.playerLimit.join("-") }
            </Label>

            <span class="separator-v" />

            { __("game-id") }:
            <Label size="big">
              { sharedObj.gameid }&nbsp;
              <a href={ "javascript:window.copy('"+
                  window.location.href+
                  "');" }>
                <i class="icon fas fa-link"></i>
              </a>
            </Label>
          </div>
        </Container>

        { /* Character selection */ }
        <Segment inverted class="charSelect nopadding"
            style={{ height: (+!state.ready)+"00%" }}>
          <CharacterSelection onSelect={ (v)=>this.onCharSelect(v) } />
        </Segment>

        { /* Waiting for players screen */ }
        <Container class="charSelect" textAlign="center"
            style={{ height: (state.ready*5)+"0%" }}>
          <Segment basic>
            <Header size="large" icon>
              <i class="icon fas fa-hourglass-start" />
              <Header.Content>
              { !playerLimitFullfilled ?
                <Fragment>
                  { __("waiting-more-players") }
                  <Header.Subheader>
                    { needPlayers } { __("x-more-need-players") }
                  </Header.Subheader>
                </Fragment>
              : (!playersReady ?
                <Fragment>
                  { __("waiting-ready") }
                  <Header.Subheader>
                    { __("arent-ready-yet") }:
                    { " "+playersUnready.map(v=>v.nickname).join(", ") }
                  </Header.Subheader>
                </Fragment>
              :
                __("waiting-server")
              )}
              </Header.Content>
            </Header>
          </Segment>
          <Container text>
            <Button size="large" labelPosition="left" inverted floated="left"
                onClick={ ()=>onButton("exit2menu") } icon basic color="red">
              <i class="fas fa-door-open icon"></i>
              { __("cancel") }
            </Button>
            <Button size="large" inverted onClick={ ()=>this.onReady(false) }
                 floated="right" disabled={ playerCountReady }>
              { __("change-character") }
            </Button>
          </Container>
        </Container>

        <Container>
          { state.ready ? "" :
            <Fragment>
              <Button size="large" labelPosition="left" inverted floated="left"
                  onClick={ ()=>onButton("exit2menu") } icon basic color="red">
                <i class="fas fa-door-open icon"></i>
                { __("cancel") }
              </Button>

              <span style={{ float: "right" }}>

                  <Fragment>
                    <Button basic disabled floated="left">
                      { state.selectedChar ? "" : __("choose-char") }
                    </Button>

                    <Button size="large" labelPosition="right" icon positive
                        disabled={ !state.selectedChar } onClick={ ()=>this.onReady() }>
                      { __("ready") }
                      <i class="fas fa-arrow-right icon"></i>
                    </Button>
                  </Fragment>
              </span>
            </Fragment>
          }
        </Container>
      </MainScreen>
    );
  }
}
