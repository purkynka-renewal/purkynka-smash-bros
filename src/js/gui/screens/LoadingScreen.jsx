import MainScreen from './_MainScreen.jsx';
import { Loader } from 'semantic-ui-react';
import { __ } from '/lang';

export default function LoadingScreen(){
  return (
    <MainScreen style={{ alignItems: "center", justifyContent: "center" }}>
      <Loader indeterminate active size="huge">{ __("Loading") }...</Loader>
    </MainScreen>
  );
}
