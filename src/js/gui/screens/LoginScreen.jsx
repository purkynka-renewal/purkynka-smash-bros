import { Component } from 'preact';
import MainScreen from './_MainScreen.jsx';
import { Input, Button } from 'semantic-ui-react';
import { __ } from '/lang';

export default class LoginScreen extends Component {
  constructor(props) {
    super(props);
    this.state = { value: window.localStorage.getItem("nickname") };
  }

  onSubmit(event) {
    event.preventDefault();
    if (!this.state.value) return;
    (this.props.onButton || (()=>{}))("login", this.state.value);
    window.localStorage.setItem("nickname",  this.state.value);
  }

  onInput(event) {
    this.setState({ value: event.currentTarget.value });
  }

  render(_, state) {
    return (
      <MainScreen style={{ alignItems: "center", justifyContent: "center" }}>
        <form action="#" onSubmit={ (ev)=>this.onSubmit(ev) }>
          <Input placeholder={ __("nickname") } size="big" action
              onChange={ (ev)=>this.onInput(ev) }>
            <input value={ state.value } />
            <Button type="submit">{ __("go") }</Button>
          </Input>
        </form>
      </MainScreen>
    );
  }
}
