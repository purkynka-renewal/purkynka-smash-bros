import MainScreen from './_MainScreen.jsx';
import MainButton from '../components/MainButton.jsx';
import MainInputButton from '../components/MainInputButton.jsx';
import { Header, Container, Input } from 'semantic-ui-react';
import { __ } from '/lang';

export default function TitleScreen({ CONFIG, onButton }){
  const { sitename } = CONFIG;
  return (
    <MainScreen>
      <Header class="sitename" size="huge" style={{ margin: "16px auto" }}>
        { sitename }
      </Header>

      <Container text id="mainMenu">
        <MainInputButton onClick={ (id)=>onButton("join", id) }
            placeholder={ __("game-id") }>
          { __("join-a-game") }
        </MainInputButton>

        <MainButton onClick={ ()=>onButton("create") }>
          { __("create-a-game") }
        </MainButton>

        <MainButton onClick={ ()=>onButton("settings") }>
          { __("settings") }
        </MainButton>
      </Container>
    </MainScreen>
  );
}
