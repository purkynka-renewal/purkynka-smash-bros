import { Component } from 'preact';
import { SemanticToastContainer } from 'react-semantic-toasts';
import { ScreenSwitcher, Screen } from './ScreenSwitcher.jsx';
import LoginScreen from './screens/LoginScreen.jsx';
import LoadingScreen from './screens/LoadingScreen.jsx';
import TitleScreen from './screens/TitleScreen.jsx';
import GamestartScreen from './screens/GamestartScreen.jsx';
import HUD from './screens/HUD.jsx';
import { __ } from '/lang';

const sharedObj = require('/js/SharedObject');
const defaultScreen = "title";

export default class App extends Component {
  constructor(props){
    super(props);
    this.config = props.CONFIG;
    this.state = {
      currentScreen: "loading",
    };
    this.handlers = {};
  }

  componentDidMount() {
    sharedObj.trigger("guiReady");

    this.handlers.setReady = this.setReady.bind(this);
    this.handlers.changeScreen = this.changeScreen.bind(this);
    this.handlers.onPopstate = this.onPopstate.bind(this);

    sharedObj.on("ready", this.handlers.setReady);
    sharedObj.on("changeScreen", this.handlers.changeScreen);
    window.addEventListener("popstate", this.handlers.onPopstate);

    // Change to loading again to initialize healthy life cycle of LoadingScreen
    this.changeScreen("loading");
  }

  componentWillUnmount() {
    sharedObj.off("ready", this.handlers.setReady);
    sharedObj.off("changeScreen", this.handlers.changeScreen);
    window.removeEventListener("popstate", this.handlers.onPopstate);

    delete this.handlers.setReady;
    delete this.handlers.changeScreen;
    delete this.handlers.onPopstate;
  }

  setReady(isReady=true) {
    if (!isReady) return this.changeScreen();
    this.changeScreen(false);
  }

  componentDidUpdate() {
    const { currentScreen } = this.state;
    if (currentScreen == "loading") return;

    const isDefault = currentScreen == defaultScreen;
    const screenTitle = isDefault ? "" : currentScreen;
    const title = [
      __(screenTitle).capitalize(),
      this.config.sitename
    ].filter(v=>v).join(" | ");

    window.history.pushState({ currentScreen }, title/*, "#"+screenTitle*/);
    document.title = title;
  }

  changeScreen(type="loading") {
    this.setState({ currentScreen: type ? type : defaultScreen });
  }

  onButton(type, ...args) {
    sharedObj.trigger("button_"+type, ...args);
  }

  onPopstate(state) {
    if (state) return;
    this.setState(state);
  }

  render(props, state) {
    return (
      <div id="gui">
        <SemanticToastContainer />
        <ScreenSwitcher currentScreen={ state.currentScreen }>
          <Screen name="loading"><LoadingScreen /></Screen>

          <Screen name="login">
            <LoginScreen onButton={ (...a)=>this.onButton(...a) } />
          </Screen>

          <Screen name="title">
            <TitleScreen CONFIG={ props.CONFIG }
                onButton={ (...a)=>this.onButton(...a) } />
          </Screen>

          <Screen name="gamestart">
            <GamestartScreen CONFIG={ props.CONFIG }
                onButton={ (...a)=>this.onButton(...a) } />
          </Screen>

          <Screen name="ingame" class="hud">
            <HUD onButton={ (...a)=>this.onButton(...a) }/>
          </Screen>
        </ScreenSwitcher>
      </div>
    );
  }
}
