const { toast } = require("react-semantic-toasts");

Array.prototype.shuffle = function(){
  let m = this.length, t, i;

  // While there remain elements to shuffle…
  while (m) {
    // Pick a remaining element…
    i = Math.floor(Math.random() * m--);

    // And swap it with the current element.
    t = this[m];
    this[m] = this[i];
    this[i] = t;
  }

  return this;
}

Array.fromb2Vec2 = function(vec) {
  return [ vec.get_x(), vec.get_y() ];
}

String.prototype.capitalize = function(){
  return (this[0] || "").toUpperCase()+this.substr(1);
}

window.notify = Object.assign(
  function (title, content, icon, color){
    toast({color, title: title, icon, description: content});
  }, {
    error(title, content){
      return this(title, content, "fas fa-exclamation-triangle", "red");
    },
    warn(title, content){
      return this(title, content, "fas fa-exclamation", "orange");
    },
    info(title, content){
      return this(title, content, "fas fa-info", "teal");
    },
    success(title, content){
      return this(title, content, "fas fa-check", "green");
    },
  }
);

function fallbackCopyTextToClipboard(text) {
  var textArea = document.createElement("textarea");
  textArea.value = text;

  // Avoid scrolling to bottom
  textArea.style.top = "0";
  textArea.style.left = "0";
  textArea.style.position = "fixed";

  document.body.appendChild(textArea);
  textArea.focus();
  textArea.select();

  try {
    var successful = document.execCommand('copy');
    var msg = successful ? 'successful' : 'unsuccessful';
  } catch (err) {
    console.error('Fallback: Oops, unable to copy', err);
  }

  document.body.removeChild(textArea);
}
window.copy = function copyTextToClipboard(text) {
  if (!navigator.clipboard) {
    fallbackCopyTextToClipboard(text);
    return;
  }
  navigator.clipboard.writeText(text).then(function() {}, function(err) {
    console.error('Async: Could not copy text: ', err);
  });
}
