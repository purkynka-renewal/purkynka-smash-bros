const io = require("socket.io-client");
const { __ } = require("/lang");
const sharedObj = require('/js/SharedObject');

class ServerManager {
  constructor() {
    // this.color = null;
    this.nickname = null;
    this.socket = null;
    this.failed = false;
    this.syncRate = null;
    this.syncInterval = null;
    this.gameRunning = false;

    this.playerID = null;
    this.player = null;
    this.players = [];
    this.isHost = false;
    this.selectedChar = null;
    this.sessionReady = false;
  }

  async create() {
    return fetch("/session", {method: "POST"}).then(res=>res.json())
        .then((result)=>{
      if (result.error) throw new Error(__("error")+": "+result.error);
      return result.id;
    }).catch((err)=>{
      window.notify.error(err.name, err.message);
      console.error(err);
    });
  }

  async join(id) {
    if (!id) return;
    this.disconnect();

    this.socket = io("/", {
      query: {
        nickname: this.nickname,
        sessionID: id,
      },
      path: "/socket",
      transports: ["websocket"],
      reconnection: false,
    });

    const updateGameStart = () => {
      if (this.selectedChar && this.sessionReady && !this.gameRunning) {
        this.gameRunning = true;
        sharedObj.trigger("game_start", this.players);
        updateSyncInterval();
      }
    };

    const updateSyncInterval = () => {
      if (this.syncInterval) return;
      this.syncInterval = setInterval(() => {
        if (!sharedObj.game) return;
        this.socket.emit("controlsUpdate", sharedObj.game.controls);

        if (this.isHost) {
          // Send game data
          this.socket.emit("gameData", {
            // Parse player data
            players: Object.entries(sharedObj.game.players).reduce((a, c) => {
              return Object.assign(a, { [c[0]]: c[1].exportData() });
            }, {}),
          });
        }
      }, 1000 / this.syncRate);
    }

    // GUI listeners
    sharedObj.on("selectedChar", (charName) => {
      this.selectedChar = charName;
      this.socket.emit("selectedChar", charName);
      updateGameStart();
    });

    // Socket listeners
    this.socket
      /// on GameInfo
      .on("gameinfo", (data) => {
        if ("ready" in data) this.sessionReady = data.ready;
        if ("playerID" in data) {
          this.playerID = data.playerID;
        }
        if ("syncRate" in data) {
          this.syncRate = data.syncRate;
        }
        if (this.playerID) {
          this.player = data.players[this.playerID];
          this.player.isCurrentPlayer = true;
          if ("isHost" in this.player) {
            sharedObj.game.isHost = this.isHost = this.player.isHost;
          }
        }
        this.players = data.players;
        sharedObj.trigger("gameinfo_update", data);
        updateGameStart();
        if (this.sessionReady) this.gameSync(data.players);
      })
      /// on fail/error
      .on("fail", (err) => {
        window.notify.error(__(err));
        this.failed = true;
      })
      .on("error", (err) => {
        window.notify.error(__(err.name), __(err.message));
        this.failed = true;
      })
      /// once disconnect
      .once("disconnect", reason => {
        if (reason != "io client disconnect") {
          window.notify.warn(__("connection-lost"));
        }
        sharedObj.trigger("button_exit2menu");
      });

    return new Promise(resolve => this.socket.once("connect", resolve));
  }

  gameSync(players) {
    if (sharedObj.game) sharedObj.game.gameSync(players);
  }

  disconnect() {
    if (this.socket) {
      this.socket.disconnect();
      this.socket = null;
    }
    if (this.syncInterval) clearInterval(this.syncInterval);
    this.syncInterval = null;
    this.gameRunning = false;

    this.playerID = null;
    this.player = null;
    this.players = [];
    this.isHost = false;
    this.selectedChar = null;
    this.sessionReady = false;
  }
}

module.exports = new ServerManager();
