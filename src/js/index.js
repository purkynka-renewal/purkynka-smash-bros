require('react-semantic-toasts/styles/react-semantic-alert.css');
require('/css/libs-fontawesome.scss');
require('/css/libs-semantic.scss');
require('/css/overrides.scss');
require('/css/style.scss');

const { hydrate, h } = require('preact');
const { render } = require('preact-render-to-string');

const CONFIG = require('/../config.json');
if (CONFIG.debug) global.debug = true;
require('./utils.js');
const App = require('./gui/App.jsx').default;
const gameData = require('/data');
const Game = require('./Game');
const Lang = require("/lang");
const sharedObj = require('./SharedObject');
const server = require('./ServerManager');
sharedObj.gameData = gameData;

//
// Parse gamedata
//
/// Max Character Stats
gameData.maxStats = {};

/// Characters
for (const [charName, character] of Object.entries(gameData.characters)) {
  // Resolve name
  character.parameters.name = charName;

  // Find max stats
  for (const [name, value] of Object.entries(character.stats)) {
    if (isNaN(value)) continue;
    if (value > (gameData.maxStats[name] || 0)) gameData.maxStats[name] = value;
  }

  // Resolve model sources
  for (const model of Object.values(character.models)) {
    model.src = CONFIG.modelsPath
        .replace("[type]", "characters")
        .replace("[name]", character.parameters.name) +
        model.src;
  }
}

//
// App
//
const app = h(App, {CONFIG});

// Normal mode
if (document.visibilityState !== "prerender") {
  const game = sharedObj.game = new Game(gameData);
  if (CONFIG.debug) global.game = game;

  // on login button
  sharedObj.on("button_login", async (nickname)=>{
    server.nickname = nickname;
    sharedObj.trigger("ready");
    if (window.location.hash) {
      const gameid = window.location.hash.substr(1);
      if (gameid) sharedObj.trigger("button_join", gameid);
    }
  });

  // on create button
  sharedObj.on("button_create", async ()=>{
    sharedObj.trigger("button_join", await server.create());
  });

  // on join button
  sharedObj.on("button_join", async (gameid)=>{
    if (!gameid) return;

    await server.join(gameid);
    sharedObj.gameid = gameid;
    sharedObj.trigger("changeScreen", "gamestart");
    window.location.hash = "#"+gameid;
  });

  // on start button
  sharedObj.on("game_start", (players)=>{
    sharedObj.trigger("ready", false);

    game.init(players).then(() => {
      game.start();

      setTimeout(() => sharedObj.trigger("changeScreen", "ingame"));
    });

    game.canvas.id = "gameScreen";
    document.body.appendChild(game.canvas);
    game.canvas.className = "";
  });

  sharedObj.on("changeScreen", (status) => {
    if (status === false) {
      window.location.hash = "";
    }
  });

  // on exit button
  sharedObj.on("button_exit2menu", ()=>{
    server.disconnect();

    game.canvas.className = "hide";
    game.stop();

    setTimeout(()=>sharedObj.trigger("changeScreen", false), 1);
  });

  (async () => {
    await Lang.getLanguage();

    hydrate(app, document.body);

    // All is ready
    sharedObj.trigger("changeScreen", "login");
  })();

  // register serviceworker
  if ('serviceWorker' in navigator) {
    // navigator.serviceWorker.register('/service-worker.js');
  }
}

// Prerender mode
else {
  Lang.getLanguage().then(() => {
    document.body.innerHTML += render(app);
    window.prerendered();
  });
}
