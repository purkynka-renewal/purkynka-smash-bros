const Entity = require("../Entity");
const { b2Vec2, b2CircleShape } = require("box2d.js");
const Rectangle = require("../../helpers/Rectangle");

module.exports = class BasicPlayerEntity extends Entity {
  constructor(world, pos=[0,0], {parameters, models, stats}) {
    const [width, height] = parameters.hitbox;
    super(world, pos, "dynamicBody", new Rectangle(width, height), 10, .5);
    this.stats = stats;
    this.parameters = parameters;
    this.width = width;
    this.height = height;
    this.models = {...models};
    this.currentModel = "default";
    this.grounded = false;
    this.canDoublejump = true;
    this.facingDirection = 1;

    this.body.SetFixedRotation(true);

    /// Foot sensor
    const circleShape = new b2CircleShape();
    circleShape.set_m_radius(width / (3 * this.worldScale));
    circleShape.set_m_p(new b2Vec2(0, -height / 2 / this.worldScale));
    this.footSensor = this.body.CreateFixture(circleShape, 0);
    this.footSensor.SetSensor(true);
    this.footSensor.name = "FOOT_SENSOR";

    /// Hand sensors
    circleShape.set_m_radius(height / (8 * this.worldScale));
    // Right
    circleShape.set_m_p(new b2Vec2(
      -parameters.handPos[0] / this.worldScale,
      -parameters.handPos[1] / this.worldScale
    ));
    this.handSensorR = this.body.CreateFixture(circleShape, 0);
    this.handSensorR.SetSensor(true);
    this.handSensorR.name = "HAND_SENSOR";
    this.handSensorR.isActive = true;
    // Left
    circleShape.get_m_p().set_x(parameters.handPos[0] / this.worldScale);
    this.handSensorL = this.body.CreateFixture(circleShape, 0);
    this.handSensorL.SetSensor(true);
    this.handSensorL.name = "HAND_SENSOR";
    this.handSensorL.isActive = false;

    /// Load textures
    this.isLoaded = false;
  }

  load() {
    const toLoad = Object.keys(this.models);
    return new Promise(resolve => {
      for (let [modelName, model] of Object.entries(this.models)) {
        this.models[modelName] = model = { ...model };
        model.image = new Image();
        model.image.src = model.src;
        model.image.decode().then(()=>{
          // Pre-calculate target image size
          const [origWidth, origHeight] = model.size;
          let width = origWidth == "auto" ? model.image.naturalWidth : origWidth;
          let height = origHeight == "auto" ? model.image.naturalHeight : origHeight;
          if (origWidth == "auto") width *= height / model.image.naturalHeight;
          if (origHeight == "auto") height *= width / model.image.naturalWidth;
          model.size = [width, height];

          // Prerender the image (useful for SVGs) (consumes more mem, I guess?)
          if (model.prerender) {
            const canvas = document.createElement("canvas");
            canvas.width = width;
            canvas.height = height;
            const ctx = canvas.getContext("2d");
            ctx.drawImage(model.image, 0, 0, width, height);
            model.image = canvas;
          }

          // Image ready
          toLoad.splice(toLoad.indexOf(modelName), 1);
          if (!toLoad.length) {
            this.isLoaded = true;
            resolve();
          }
        });
      }
    });
  }

  applyControls(controls) {
    this.move(controls.left - controls.right);
    if (controls.jump) this.jump();
    if (controls.smash) this.smash();
    controls.smash = controls.jump = false;
    return controls;
  }

  exportData() {
    return {
      position: Array.fromb2Vec2(this.pos),
      velocity: Array.fromb2Vec2(this.body.GetLinearVelocity()),
      params: {
        currentModel: this.currentModel,
        facingDirection: this.facingDirection,
      },
    };
  }

  importData(data) {
    if ("position" in data) {
      this.body.SetTransform(new b2Vec2(...data.position), 0);
    }
    this.body.SetLinearVelocity(new b2Vec2(...data.velocity));
    Object.assign(this, data.params);
  }

  setGrounded(grounded) {
    this.grounded = grounded;
    if (grounded) this.canDoublejump = true;
  }

  hold(state=true){
    const velocity = this.body.GetLinearVelocity();
    if (!state || velocity.get_y() > 0) {
      this.setGrounded(false);
      return this.body.SetGravityScale(1);
    }

    this.body.SetGravityScale(0.01);
    velocity.set_y(0);
    this.body.SetLinearVelocity(velocity);
    this.setGrounded(true);
  }

  move(direction) {
    if (!direction) return;

    this.facingDirection = -direction;
    this.handSensorL.isActive =
        !(this.handSensorR.isActive = this.facingDirection > 0);

    const force = this.stats[this.grounded ? "speed" : "glide"];
    this.body.ApplyLinearImpulse(
      new b2Vec2(force * direction * this.body.GetMass(), 0),
      this.body.GetWorldCenter(),
      true
    );
  }

  jump() {
    if (!this.grounded && !this.canDoublejump) return;
    let force = this.stats.jump;

    // Partially clear all veloctities
    const velocity = this.body.GetLinearVelocity();
    velocity.set_y(velocity.get_y() * .1);


    // Double jump
    if (!this.grounded) {
      velocity.set_x(velocity.get_x() * .5);
      this.canDoublejump = false;
    }

    this.body.SetLinearVelocity(velocity);

    this.body.ApplyLinearImpulse(
      new b2Vec2(0, force * this.body.GetMass()),
      this.body.GetWorldCenter(),
      true
    );
  }

  // opposite of jump
  smash() {
    this.body.ApplyLinearImpulse(
      new b2Vec2(0, -this.stats.smash * this.body.GetMass()),
      this.body.GetWorldCenter(),
      true
    );
  }

  render(ctx) {
    const _pos = this.body.GetWorldCenter();
    const angle = this.body.GetAngle();
    const pos = [_pos.get_x() *this.worldScale, _pos.get_y() *this.worldScale];
    const model = this.models[this.currentModel], [width, height] = model.size;

    let shouldFlip = this.facingDirection < 0;
    if (model.flipped) shouldFlip = !shouldFlip;

    ctx.beginPath();
    ctx.translate(-pos[0], -pos[1]);
    if (shouldFlip) ctx.scale(-1, 1);
    ctx.rotate(angle);
    if (this.isLoaded) {
      ctx.drawImage(
        model.image,
        -this.width / 2 + (model.offset[0] || 0),
        -this.height / 2 + (model.offset[1] || 0),
        width,
        height,
      );
    } else {
      const points = this.shape.points.map(item => ([
        item[0],
        item[1]
      ]));
      ctx.moveTo(...points[points.length-1]);
      for (const point of points) {
        ctx.lineTo(...point);
      }
      ctx.strokeStyle = "cyan";
      ctx.stroke();
    }
    ctx.rotate(-angle);
    if (shouldFlip) ctx.scale(-1, 1);
    ctx.translate(pos[0], pos[1]);
  }
};
