const Entity = require("./Entity");
const Rectangle = require("../helpers/Rectangle");

module.exports = class FloorEntity extends Entity {
  constructor(world, pos=[0,0], width=1, height=1) {
    super(world, pos, null, new Rectangle(width, height), 0, 1.0);
  }

  render(ctx) {
    ctx.strokeStyle = "white";
    super.render(ctx);
  }
};
