const Box2D = require("box2d.js"), { b2BodyDef, b2Vec2 } = Box2D;
const { worldScale } = require("../../../../config.json");

module.exports = class Entity {
  constructor(world, pos, type, shape, density=0, friction=0.2) {
    this.shape = shape;
    this.worldScale = worldScale;
    this.bodyDef = new b2BodyDef();
    if (pos) this.bodyDef.set_position(new b2Vec2(...pos.map(v=>v/worldScale)));
    if (type) this.bodyDef.set_type(Box2D["b2_"+type]);
    this.body = world.CreateBody(this.bodyDef);
    this.body.entity = this;
    this.body.SetLinearDamping(0.1);
    this.pos = this.body.GetPosition();
    this.fixture = this.body.CreateFixture(shape, density);
    this.fixture.SetFriction(friction);
    // this.fixture.SetFilterData(new b2Filter(0x0001, 0x0000-0x0001));
  }

  update(delta) { }

  render(ctx) {
    const _pos = this.body.GetWorldCenter();
    const angle = this.body.GetAngle();
    const pos = [_pos.get_x() * worldScale, _pos.get_y() * worldScale];
    const points = this.shape.points.map(item => ([
      item[0],
      item[1]
    ]));

    ctx.beginPath();
    ctx.translate(-pos[0], -pos[1]);
    ctx.rotate(angle);
    ctx.moveTo(...points[points.length-1]);
    for (const point of points) {
      ctx.lineTo(...point);
    }
    ctx.rotate(-angle);
    ctx.translate(pos[0], pos[1]);

    ctx.stroke();
  }
}
