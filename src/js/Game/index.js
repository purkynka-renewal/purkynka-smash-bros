const ContactListener = require("./ContactListener");
const FloorEntity = require("./entities/FloorEntity");
const playerEntities = require("./entities/playerEntities");
const CONFIG0 = require("../../../config.json");

const Box2D = require("box2d.js");
const { b2Vec2, b2World } = Box2D;

const CONFIG = {
  ...CONFIG0,
  noVsync: false,
  timeStep: 1 / CONFIG0.expectedFrameRate,
  bgColor: window.getComputedStyle(document.body, null)
      .getPropertyValue("background-color"),
};

if (global.debug) global.gameConfig = CONFIG;

module.exports = class Game {
  ////
  // INIT
  ////
  constructor(gameData) {
    this.gameData = gameData;
    this.animFrame = null;
    this.shouldRun = false;
    this.canvasDef = [0, 0, 0, 0];
    this.isHost = false;
    this.controls = {
      jump: false,
      smash: false,
      right: false,
      left: false,
    };

    /// Canvas
    this.canvas = document.createElement("canvas");
    /// Context
    this.ctx = this.canvas.getContext("2d", {
      // alpha: false
    });
    this.ctx.save();

    /// Start
    this.sizeCanvas();

    // EVENTS
    // resize
    window.addEventListener("resize", () => this.sizeCanvas());
    const handleKeyPress = (event) => {
      const pressed = event.type == "keydown";
      switch (event.key) {
        case "ArrowUp":
          this.controls.jump = pressed;
          break;
        case "ArrowDown":
          this.controls.smash = pressed;
          break;
        case "ArrowRight":
          this.controls.right = pressed;
          break;
        case "ArrowLeft":
          this.controls.left = pressed;
          break;
      }
    };
    document.addEventListener("keydown", handleKeyPress);
    document.addEventListener("keyup", handleKeyPress);
  }

  ////
  // (RE)INIT WORLD
  ////
  async init(players) {
    this.world = new b2World(new b2Vec2(...CONFIG.gravity));
    this.world.SetAutoClearForces(false);
    this.world.SetContactListener(ContactListener);
    this.entities = [];
    this.players = {};
    this.player = null;
    const loadPromises = [];

    for (const [playerID, playerData] of Object.entries(players)) {
      const characterData = this.gameData.characters[playerData.selectedChar];
      const player = new playerEntities[characterData.entityTemplate](
        this.world,
        [-1920/3, -1080/3],
        characterData
      );
      this.players[playerID] = player;
      if (playerData.isCurrentPlayer) this.player = player;
      loadPromises.push(player.load());
    }

    this.entities.push(new FloorEntity(this.world, [-1920/2, -1000], 1920, 75));
    this.entities.push(new FloorEntity(this.world, [-1920/6, -600], 1920/3, 75));
    this.entities.push(new FloorEntity(this.world, [-1920/6*5, -600], 1920/3, 50));
    this.entities.push(new FloorEntity(this.world, [-1920/2, -300], 1920/5, 20));

    return Promise.all(loadPromises);
  }

  ////
  // SIZE CANVAS/CAMERA
  ////
  sizeCanvas(){
    const [ targetWidth, targetHeight ] = CONFIG.referenceScreen;
    let { innerWidth, innerHeight } = window;
    innerWidth *= CONFIG.resolution;
    innerHeight *= CONFIG.resolution;

    this.canvas.width = innerWidth;
    this.canvas.height = innerHeight;

    const scaleRatios = [
      innerWidth / targetWidth,
      innerHeight / targetHeight
    ];
    let scaleRatio = Math.min(...scaleRatios);
    let realWidth = innerWidth / scaleRatio;
    let realHeight = innerHeight / scaleRatio;
    const offset = [0, 0];

    /// calculate camera
    // distance from center
    if (this.player) {
      const center = [-targetWidth / 2, -targetHeight / 2];
      const mapRadius = -Math.max(...center) * .75;
      const distanceX = center[0] - this.player.pos.get_x() * CONFIG.worldScale;
      const distanceY = center[1] - this.player.pos.get_y() * CONFIG.worldScale;

      const distanceScaleX =
          Math.min(1, (Math.abs(distanceX) + center[0] / 4) / mapRadius / 2);
      offset[0] = (distanceX / 2) * distanceScaleX;
      if (Math.sign(distanceX) != Math.sign(offset[0])) offset[0] = 0;
      const distanceScaleY =
          Math.min(1, (Math.abs(distanceY) + center[1] / 4) / mapRadius / 2);
      offset[1] = (distanceY / 2) * distanceScaleY;
      if (Math.sign(distanceY) != Math.sign(offset[1])) offset[1] = 0;

      let cameraScale = 1 - Math.max(0, distanceScaleX, distanceScaleY) / 3;
      cameraScale = Math.max(CONFIG.maxZoom, cameraScale);
      scaleRatio *= cameraScale;
      realWidth /= cameraScale;
      realHeight /= cameraScale;
    }

    this.canvasDefs = [
      (realWidth  - targetWidth ) / 2 - offset[0],
      (realHeight - targetHeight) / 2 - offset[1],
      realWidth, realHeight
    ];

    this.ctx.restore();
    this.ctx.scale(scaleRatio, scaleRatio);
    this.ctx.translate(...this.canvasDefs);
  }

  ////
  // STOP
  ////
  stop() {
    this.shouldRun = false;
    if (this.animFrame) {
      window.clearTimeout(this.animFrame);
      window.cancelAnimationFrame(this.animFrame);
    }
    this.animFrame = null;
  }

  ////
  // GAME SYNC
  ////
  gameSync(players) {
    for (const [playerID, player] of Object.entries(players)){
      // If host, update controls for players
      if (this.isHost && playerID != this.playerID) {
        this.players[playerID].applyControls(player.controls);
      }
      // Else import data from host
      else if (!this.isHost) {
        this.players[playerID].importData(player);
      }
    }
  }

  ////
  // START
  ////
  start() {
    if (this.shouldRun) return;

    performance.mark("render");
    let accumulator = 0;
    this.shouldRun = true;

    const render = () => {
      performance.measure("renderTime", "render")
      const delta = performance.getEntriesByName("renderTime")[0].duration/1000;
      performance.clearMeasures("renderTime");
      performance.clearMarks("render");

      if (!this.shouldRun) return;

      if (this.isHost || CONFIG.noVsync) {
        this.animFrame = window.setTimeout(render);
      }
      else this.animFrame = window.requestAnimationFrame(render);

      performance.mark("render");

      //
      // Update
      //
      accumulator += Math.min(CONFIG.timeStep, delta);

      while (accumulator >= CONFIG.timeStep) {
        // This is just a prediction if not host
        this.player.applyControls(this.controls);

        // Simulate
        this.entities.forEach(entity => entity.update(CONFIG.timeStep));
        this.world.Step(CONFIG.timeStep, 10, 20);

        accumulator -= CONFIG.timeStep;
      }

      this.world.ClearForces();

      // RESPAWN
      for (const player of Object.values(this.players)) {
        if (player.pos.get_y() * CONFIG.worldScale < -2000) {
          const { body } = this.player;
          body.SetTransform(
            new b2Vec2(-1920/3/CONFIG.worldScale, -1080/3/CONFIG.worldScale),
            0.0
          );
          body.SetLinearVelocity(new b2Vec2(0, 0));
          body.SetAwake(1);
          body.SetActive(1);
        }
      }

      //
      // Render
      //
      if (this.player.parameters.name != "karel") {
        this.sizeCanvas();
        this.ctx.fillStyle = CONFIG.bgColor;
        this.ctx.fillRect(
          -this.canvasDefs[0], -this.canvasDefs[1],
          this.canvasDefs[2], this.canvasDefs[3]
        );
      }

      this.entities.forEach(entity => entity.render(this.ctx));
      Object.values(this.players).forEach(player => player.render(this.ctx));
    }

    render();
  }
};

module.exports.CONFIG = CONFIG;
