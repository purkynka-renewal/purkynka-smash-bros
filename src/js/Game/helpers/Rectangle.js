const { b2Vec2, b2PolygonShape } = require("box2d.js");
const { worldScale } = require("../../../../config.json");

module.exports = class Rectangle extends b2PolygonShape {
  constructor(width, height){
    super();
    this.SetAsBox(width / 2 / worldScale, height / 2 / worldScale);
    this.points = [
      [-width / 2, -height / 2],
      [width / 2, -height / 2],
      [width / 2, height / 2],
      [-width / 2, height / 2]
    ];
  }
};
