const { JSContactListener, wrapPointer, b2Contact } = require("box2d.js");

module.exports = new JSContactListener();

module.exports.EndContact = module.exports.BeginContact = function(contact) {
  contact = wrapPointer(contact, b2Contact);
  const isTouching = !!contact.IsTouching();

  for (const fixture of [contact.GetFixtureA(), contact.GetFixtureB()]) {
    if (!fixture.name) continue;
    const entity = fixture.GetBody().entity;

    switch (fixture.name) {
      case "FOOT_SENSOR":
        entity.setGrounded(isTouching);
        break;

      case "HAND_SENSOR":
        if (!isTouching) entity.hold(false);
        else if (fixture.isActive) entity.hold(true);
        break;
    }
  }
};

module.exports.PreSolve = function() {};

module.exports.PostSolve = function() {};
