module.exports = {
  waiters: {},

  on(eventType, handler) {
    if (!(eventType in this.waiters)) this.waiters[eventType] = [];
    this.waiters[eventType].push(handler);
  },

  once(eventType, handler) {
    const _handler = (...args) => {
      this.off(eventType, _handler);
      return handler(...args);
    };
    this.on(eventType, _handler);
  },

  off(eventType, handler) {
    const waiters = this.waiters[eventType];
    waiters.splice(waiters.indexOf(handler), 1);
  },

  trigger(eventType, ...data) {
    if (!(eventType in this.waiters)) return;
    this.waiters[eventType].forEach(handler => handler(...data));
  },
};
