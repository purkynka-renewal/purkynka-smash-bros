const DEFAULT_LANG = "en";

class Lang {
  constructor(){}

  translate(str){
    if (this.language) {
      const query = (str || "").toLowerCase().trim();
      if (query && query in this.language) {
        return this.language[query];
      }
    }
    return str;
  }

  getLanguage(languages=[]) {
    if (!languages) languages = [];
    if (navigator in window) {
      if (language in navigator) languages.push(navigator.language);
      if (languages in navigator) languages.push(...navigator.languages);
    }

    for (const lang of [...languages, DEFAULT_LANG]) {
      if (lang in this) return this._getLanguage(lang);
    }
  }
  async _getLanguage(lang) {
    this.langName = lang;
    this.language = (await this[lang]()).default;
    return this.language;
  }

  // Add language imports here
  en() { return import("./en.yaml") }
};

const lang = module.exports = new Lang();
lang.__ = Object.assign(lang.translate.bind(lang), {
  getLanguage(){ return lang.langName; },
  DEFAULT_LANG,
});
