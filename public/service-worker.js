/**
 * Copyright 2018 Google Inc. All Rights Reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// If the loader is already loaded, just stop.
if (!self.define) {
  const singleRequire = name => {
    if (name !== 'require') {
      name = name + '.js';
    }
    let promise = Promise.resolve();
    if (!registry[name]) {
      
        promise = new Promise(async resolve => {
          if ("document" in self) {
            const script = document.createElement("script");
            script.src = name;
            document.head.appendChild(script);
            script.onload = resolve;
          } else {
            importScripts(name);
            resolve();
          }
        });
      
    }
    return promise.then(() => {
      if (!registry[name]) {
        throw new Error(`Module ${name} didn’t register its module`);
      }
      return registry[name];
    });
  };

  const require = (names, resolve) => {
    Promise.all(names.map(singleRequire))
      .then(modules => resolve(modules.length === 1 ? modules[0] : modules));
  };
  
  const registry = {
    require: Promise.resolve(require)
  };

  self.define = (moduleName, depsNames, factory) => {
    if (registry[moduleName]) {
      // Module is already loading or loaded.
      return;
    }
    registry[moduleName] = Promise.resolve().then(() => {
      let exports = {};
      const module = {
        uri: location.origin + moduleName.slice(1)
      };
      return Promise.all(
        depsNames.map(depName => {
          switch(depName) {
            case "exports":
              return exports;
            case "module":
              return module;
            default:
              return singleRequire(depName);
          }
        })
      ).then(deps => {
        const facValue = factory(...deps);
        if(!exports.default) {
          exports.default = facValue;
        }
        return exports;
      });
    });
  };
}
define("./service-worker.js",['./workbox-ef152999'], function (workbox) { 'use strict';

  /**
  * Welcome to your Workbox-powered service worker!
  *
  * You'll need to register this file in your web app.
  * See https://goo.gl/nhQhGp
  *
  * The rest of the code is auto-generated. Please don't update this file
  * directly; instead, make changes to your Workbox build configuration
  * and re-run your build process.
  * See https://goo.gl/2aRDsh
  */

  workbox.enable();
  workbox.setCacheNameDetails({
    prefix: "purkynkasmashbros"
  });
  workbox.skipWaiting();
  workbox.clientsClaim();
  /**
   * The precacheAndRoute() method efficiently caches and responds to
   * requests for URLs in the manifest.
   * See https://goo.gl/S9QRab
   */

  workbox.precacheAndRoute([{
    "url": "/boundle.min.css",
    "revision": "4649c60c0d4cb48524785959fe72010e"
  }, {
    "url": "/fonts/Azonix.otf",
    "revision": "cdfe47b31e9184a55cf02eef1baf7240"
  }, {
    "url": "/fonts/fa-brands-400.woff2",
    "revision": "f075c50f89795e4cdb4d45b51f1a6800"
  }, {
    "url": "/fonts/fa-regular-400.woff2",
    "revision": "4a74738e7728e93c4394b8604081da62"
  }, {
    "url": "/fonts/fa-solid-900.woff2",
    "revision": "8e1ed89b6ccb8ce41faf5cb672677105"
  }, {
    "url": "/img/characters/karel/karel-head.png",
    "revision": "00d1843133c3156c357751a762077242"
  }, {
    "url": "/img/characters/pikabudka/pikabudka-classic.svg",
    "revision": "595c31ed0bdf27fbc45ca720af5ac323"
  }, {
    "url": "/img/characters/pikabudka/zlaja-pikabudka.svg",
    "revision": "94c8a773be54f8003809478fc7bc3550"
  }, {
    "url": "/img/flags.png",
    "revision": "9c74e172f87984c48ddf5c8108cabe67"
  }, {
    "url": "/index.html",
    "revision": "832e425e76d8767dbdfaae66f39a3616"
  }, {
    "url": "/lang_en_yaml.boundle.min.js",
    "revision": "e427f821684f8a6bcd3db76d5dd72d0c"
  }], {});
  workbox.cleanupOutdatedCaches();
  workbox.registerRoute(/.*/, new workbox.StaleWhileRevalidate(), 'GET');

});
